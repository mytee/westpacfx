# WestpacFX


The app must be written in Swift or Kotlin - Swift <br>
The app must be written using Xcode 9+ for iOS or Android Studio for Android. -Xcode10<br>
Use of suitable design patterns, and a strict separation of concerns when developing a mobile application - done<br>
Create a library containing all functionality that could be reused by 3rd party developers in any application that requires exchange rate calculations. 3rd parties using this library should not be tightly coupled to any specific technology and should be able to consume the library in any way they choose - provided <br>
Code should be commented sufficiently to allow auto generation of library API documentation - done<br>
Use all the data the API provided - done<br>

Asynchronous development principals when retrieving and displaying data originating from network calls - done


UI interaction and data binding principals 

Sound management of User Interface - done
The application should be able to be re-branded (colours, fonts, assets) - 2 distinct branded targets/variants should be included in the project - ??<br>
The app should be built with a universal UI. - done


Correct use of the application life cycle, management of the UI thread - done?<br>

Unit tests/mocks to demonstrate the code is testable - ?? how? it is a presentation of json file from rest call. no data input, no activities. what to test?<br>

ScreenShot https://gitlab.com/mytee/westpacfx/blob/master/Simulator_Screen_Shot_-_iPhone_X%CA%80_-_2019-09-06_at_20.20.51.png