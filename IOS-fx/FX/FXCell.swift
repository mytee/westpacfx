//
//  FXCell.swift
//  FX
//
//  Created by Admin on 4/9/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class FXCell: UITableViewCell {
    @IBOutlet weak var buyttVal: UILabel!
    @IBOutlet weak var buynotesVal: UILabel!
    @IBOutlet weak var sellttVal: UILabel!
    @IBOutlet weak var buytcVal: UILabel!
    @IBOutlet weak var sellnotesVal: UILabel!
    @IBOutlet weak var currencyCode: UILabel!
    @IBOutlet weak var currencyName: UILabel!
    @IBOutlet weak var flag: UIImageView!
    @IBOutlet weak var country: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
