//
//  FXViewController.swift
//  FX
//
//  Created by Admin on 4/9/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import currency

class FXViewController: UIViewController {
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var leftMargin: NSLayoutConstraint!
    @IBOutlet weak var rightMargin: NSLayoutConstraint!
    @IBOutlet var container: UIView!
    @IBOutlet weak var lastUpdated: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    @IBAction func menuClk(_ sender: UIBarButtonItem) {
        let menu1 = UIImage(named:"icons8-menu");
        let menu2 = UIImage(named:"icons8-menu2");
        sender.image = sender.image == menu1 ? menu2 : menu1;
        
        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "id") as! PopMenu
        popvc.view.tag = 1000;
  
        
        if sender.image == menu2 {
            leftMargin.constant = 200;
            rightMargin.constant = 200;
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
                self.view.layoutIfNeeded()
            }) { (animationComplete) in
                print("Menu opened")
            }
            
  
            popvc.view.frame = CGRect(x:0, y:60, width:200, height:200);
            
            // popvc.view.frame = self.view.frame
            popvc.view.isHidden = false;
            popvc.modalPresentationStyle = .overCurrentContext
            popvc.modalTransitionStyle = .crossDissolve
   
            self.view.addSubview(popvc.view);
            self.view.bringSubviewToFront(popvc.view)
  
            
            //popvc.didMove(toParentViewController: self)
        } else {
            //popvc.view.isHidden = true;
            popvc.dismiss(animated: true, completion: nil)
            let subViews = self.view.subviews
            for subview in subViews{
                if subview.tag == 1000 {
                    //subview.removeSubviews()
                    subview.removeFromSuperview()
                }
            }
            //popvc.view.removeFromSuperview();
            leftMargin.constant = 0;
            rightMargin.constant = 0;
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseIn, animations: {
                self.view.layoutIfNeeded()
            }) { (animationComplete) in
                print("Menu closed!")
            }
        }
    }
    @IBAction func resetClk(_ sender: UIButton) {
        print("reset clicked");
        menuClk(menuBtn);
        
    }
    @IBAction func settingClk(_ sender: UIButton) {
        print("setting clicked");
        menuClk(menuBtn);
        
    }
    var cx: currency?
    var theArray: [(currencyCode: String, currencyName: String, country: String, buyTT: String, sellTT: String, buyTC: String, buyNotes: String, sellNotes: String ,lastUpdated: String)] = [];

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Westpac FX";
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg")!);
        if !NetworkModel.isConnectedToNetwork(){
           
            print("Internet Connection not Available!")
            alert(title: title!, message: "Internet is not available, app abort!") {
                exit(0);
            };
        }
        
        
        cx = currency();
        let group = DispatchGroup();
        group.enter();
        cx!.getData() { result in
            self.theArray = (self.cx?.getArray())!;
            group.leave();
        }
        group.notify(queue: DispatchQueue.main) {
            self.tableView.reloadData()
            self.spinner.stopAnimating();
        }
        
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.backgroundView = nil;
        tableView.backgroundColor = UIColor.clear;
        tableView.rowHeight = 160;
    }
}

extension FXViewController: UITabBarDelegate {
    // UITabBarDelegate
     func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("Selected item " , item.tag)
    }
    
    // UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected view controller")
    }
}

extension FXViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(theArray.count);
        return theArray.count;
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected cell number: \(indexPath.row)!");
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! FXCell;
        cell.selectionStyle = UITableViewCell.SelectionStyle.none;
        cell.currencyCode.text = theArray[indexPath.row].currencyCode;
        cell.currencyName.text = theArray[indexPath.row].currencyName;
        cell.buyttVal.text = theArray[indexPath.row].buyTT;
        cell.sellttVal.text = theArray[indexPath.row].sellTT;
        cell.buytcVal.text = theArray[indexPath.row].buyTC;
        cell.buynotesVal.text = theArray[indexPath.row].buyNotes;
        cell.sellnotesVal.text = theArray[indexPath.row].sellNotes;
        cell.country.text = theArray[indexPath.row].country;
        cell.flag.image = UIImage(named: theArray[indexPath.row].country);
        lastUpdated.text = "Last Updated " + theArray[indexPath.row].lastUpdated;
        return cell;
    }


}
