//
//  File.swift
//  FX
//
//  Created by Admin on 6/9/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit

func alert (title: String, message: String , callback:  @escaping () -> ()) {
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate;
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
        callback()
    }))
    appDelegate.window!.rootViewController?.present(alertController, animated: true, completion: nil)
    
}
