//
//  currency.swift
//  currency
//
//  Created by Admin on 30/8/19.
//  Copyright © 2019 Admin. All rights reserved.
//
import SystemConfiguration
import Foundation

public class currency {
    public init() {
        print("init");
        //getData();
    }
    
    public func getData (_ callback: @escaping (_ in: String) -> ()) {
        let path = Bundle.main.path(forResource: "localData", ofType: "plist")!
        if let dict = NSDictionary(contentsOfFile: path) {
            if let theURL = dict["WPURL"] as? String    {
                print("You're using \(theURL)");
                NetworkModel.getRequest(theUrl: theURL) {result in
                    self.freshData(jsonStr: result);
                    callback(result);
                }
            }
        }
    }
    var mData: [[String: String]] = []; // storage in dictionary format
    var length: Int { return mData.count; } // debug: array count
    var listCurencies: String { return getCurrencies() } // list of fx codes in array

    // module:addData add and sort on fxCode // internal call
    // param: val input dictioray
    // usage: addData(val: [String: String])
    // output: nill
   func addData(val: [String: String]) {
        mData.append(val);
        mData.sort(by: { ($0["currencyCode"] as! String) < $1["currencyCode"] as! String });
    }

    // Debug: list all currency code in array
    public func getCurrencies() -> String {
        var str = "";
        for s in mData {
            str += s["currencyCode"]! + ",";
        }
        str.removeLast();
        return str;
    }
    
    // module:getArray // get the array od tuple of fx
    // param:
    // usage: var outArray = getArray();
    // output:array of Tupple format of data
    public func getArray() -> [(currencyCode: String, currencyName: String, country: String, buyTT: String, sellTT: String, buyTC: String, buyNotes: String, sellNotes: String, lastUpdated: String)] {
        var theAR: [(String, String, String, String,  String,  String,  String, String, String)] = [];
        for m in mData {
            theAR.append((m["currencyCode"]!, m["currencyName"]!, m["country"]!, m["buyTT"]!, m["sellTT"]!, m["buyTC"]!, m["buyNotes"]!, m["sellNotes"]!,m["LASTUPDATED"]!));
        }
        return theAR;
    }

    // module:getTuple
    // param: code: "THB" or code: "USD"
    // usage: getTuple(code:"THB");
    // output:Tupple format of data from the currecyCode
    public func getTuple(code: String) -> (currencyCode: String, currencyName: String, country: String, buyTT: String, sellTT: String, buyTC: String, buyNotes: String, sellNotes: String, lastUpdated: String) {
        // var n = mData.firstIndex(of: ["currencyCode"  : code]);
        let index = mData.firstIndex { $0["currencyCode"] == code }!;
        let m = mData[index];
        return (code, m["currencyName"]!, m["country"]!, m["buyTT"]!, m["sellTT"]!, m["buyTC"]!, m["buyNotes"]!, m["sellNotes"]!, m["LASTUPDATED"]!)
    }

    // module:freshData(jsonStr: String) // internal call
    // param: jsonStr: result from get
    // usage: freshData(jsonStr: jsondata);
    // output: nil
    func freshData(jsonStr: String) {
        do {
            if let json = jsonStr.data(using: .utf8) {
                if let jdt = try JSONSerialization.jsonObject(with: json, options: .allowFragments) as? [String: AnyObject] {
                    let data = jdt["data"] as! [String: AnyObject]
                    let Brands = data["Brands"] as! [String: AnyObject]
                    let WBC = Brands["WBC"] as! [String: AnyObject]
                    let Portfolios = WBC["Portfolios"] as! [String: AnyObject]
                    let FX = Portfolios["FX"] as! [String: AnyObject]
                    let Products = FX["Products"] as! [String: AnyObject]
                    for e in Products {
                        //   print(e.key);
                        if e.value is [String: AnyObject] {
                            let rates = e.value["Rates"] as! [String: AnyObject]
                            let fin = rates[e.key] as! [String: String];
                            addData(val: fin);
                        }
                    }
                } else {
                    print("error");
                }
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
